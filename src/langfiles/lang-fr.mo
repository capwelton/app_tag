��          �            h     i  	   m     w          �     �     �     �     �  .   �     �     �                   ?     G     V  	   _     i     u     ~     �     �  -   �     �  )   �                                             
                            	                 Add Add a tag Checked Delete Description Edit Label No Not checked The "%s" tag has been replaced by the "%s" tag The tag has been saved The tag label must not be empty Validate Yes Project-Id-Version: apptag
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-27 15:21+0200
Last-Translator: SI4YOU <contact@si-4you.com>
Language-Team: SI4YOU<contact@si-4you.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: libapp_translate;libapp_translate:1,2;translate:1,2;translate;translatable:1,2;translatable
X-Generator: Poedit 3.1
X-Poedit-SearchPath-0: .
 Ajouter Ajouter un tag Verifié Supprimer Description Modifier Libellé Non Non vérifié Le tag "%s" a été remplacé par le tag "%s" Le tag a été enregistré Le libellé du tag ne doit pas être vide Valider Oui 