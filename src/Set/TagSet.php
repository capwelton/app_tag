<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Capwelton\App\Tag\Set;

/**
 * A class used to store tags.
 *
 * @property    ORM_StringField    $label
 * @property    ORM_StringField    $description
 * @property    ORM_BoolField      $checked
 *
 * @method app_Tag                  get(mixed $criteria)
 * @method app_Tag                  request(mixed $criteria)
 * @method app_Tag[]|\ORM_Iterator  select(\ORM_Criteria $criteria)
 * @method app_Tag                  newRecord()
 *
 * @method \Func_App App()
 */
class TagSet extends \app_TraceableRecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);

        $App = $this->App();
        $this->setTableName($App->classPrefix.'Tag');
        $this->setDescription('Tag');
        $appC = $App->getComponentByName('Tag');

        $this->addFields(
            ORM_StringField('label')->setDescription($appC->translate('Label')),
            ORM_StringField('description')->setDescription($appC->translate('Description')),
            ORM_BoolField('checked')->setDescription('Checked')->setOutputOptions($appC->translate('No'), $appC->translate('Yes'))
        );
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new TagBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new TagAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }


    /**
     * Returns an iterator of tags associated to the specified object,
     * optionally filtered on the specified link type.
     *
     * @return \ORM_Iterator
     */
    public function selectFor(\app_Record $object)
    {
        $App = $this->App();
        $rc = new \ReflectionClass($this);
        $targetClass = $App->classPrefix.$rc->getShortName();
        $targetClass = substr($targetClass, 0, -strlen(\ORM_RecordSet::SUFFIX));
        $sourceClass = $object->getClassName();
        
        $linkSet = $App->LinkSet();
        $linkSet->hasOne('targetId', $targetClass);
        $linkSet->join('targetId');
        
        $criteria = array();
        
        $criteria[] = $linkSet->sourceClass->is($sourceClass);
        $criteria[] = $linkSet->sourceId->is($object->id);
        $criteria[] = $linkSet->targetClass->is($targetClass);
        $criteria[] = $linkSet->type->is('hasTag');
        $criteria[] = $linkSet->targetId->deleted->is(0);
        
        return $linkSet->select($linkSet->all($criteria));
        return parent::selectLinkedTo($object, 'hasTag');
    }

    

    /**
     * Replaces all links to $oldTagId by links to $newTagId.
     *
     * @return tag.set.class
     */
    public function replaceLinks($oldTagId, $newTagId, $linkType = 'hasTag')
    {
        $linkSet = $this->App()->LinkSet();

        $links = $linkSet->select(
            $linkSet->targetClass->is($this->App()->TagClassName())
               ->_AND_($linkSet->targetId->is($oldTagId))
               ->_AND_($linkSet->type->is($linkType))
        );

        foreach ($links as $link) {
            $link->targetId = $newTagId;
            $link->save();
        }

        return $this;
    }

    /**
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
//         if ($this->App()->Access()->administer()) {
//             return $this->all();
//         }
//         return $this->none();
    }

    /**
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }


    /**
     * {@inheritDoc}
     * @see \app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return $this->all();
//         return $this->App()->Access()->administer();
    }
}

class TagBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class TagAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}