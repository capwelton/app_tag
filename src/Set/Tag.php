<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Tag\Set;

/**
 * @property    string     $label
 * @property    string     $description
 * @property    bool       $checked
 *
 * @method Func_App App()
 */
class Tag extends \app_TraceableRecord
{
    /**
     * {@inheritDoc}
     * @see \ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        return $this->label;
    }

    /**
     * {@inheritDoc}
     * @see \app_TraceableRecord::linkTo()
     */
    public function linkTo(\app_Record $object, $linkType = 'hasTag')
    {
        return parent::linkTo($object, $linkType);
    }


    /**
     * {@inheritDoc}
     * @see \app_TraceableRecord::unlinkFrom()
     */
    public function unlinkFrom(\app_Record $source, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();
        
        $linkSet->deleteLink($source, $this, $linkType);
        
        return $this;
    }


    /**
     * Replaces all links this tag by links to the one specified.
     *
     * @param tag.record.class|int	$tag	A tag object or id.
     *
     * @return tag.record.class
     */
    public function replaceBy($tag)
    {
        if ($tag instanceof tag) {
            $tag = $tag->id;
        }
        $this->getParentSet()->replaceLinks($this->id, $tag);
        return $this;
    }
}
