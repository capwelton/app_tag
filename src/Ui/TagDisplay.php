<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Tag\Ui;

use Capwelton;

/**
 * Display the tags associated to a app_Record.
 *
 * @extends Widget_FlowLayout
 */
class TagDisplay extends \app_UiObject
{
    /**
     * @param \Func_App			$app
     * @param array|\Iterator	$tags		Array or Iterator of app_Tag
     * @param string			$id
     */
    public function __construct($app, $tags, \app_Record $associatedObject, \Widget_Action $tagAction = null, $id = null)
    {
        parent::__construct($app);
        $W = bab_Widgets();
        
        // We simulate inheritance from Widget_FlowLayout.
        // Now $this can be used as a FlowLayout
        $this->setInheritedItem($W->FlowLayout($id));
        
        $this->setSpacing(2, 'px');
        
        $this->addClass(\Func_Icons::ICON_LEFT_SYMBOLIC);
        
        foreach ($tags as $tag) {
            /* @var $tag Capwelton\App\Entry\Set\Tag */
            if ($tag instanceof \app_Link) {
                $tag = $tag->targetId;
            }
            
            $tagLabel = $W->Label($tag->label)
            ->setTitle($tag->description);
            if (isset($tagAction)) {
                $tagLabel = $W->Link($tagLabel, $tagAction->setParameter('tag', $tag->label));
            }
            
            $tagDeleteLink = $W->Link(
                '',
                $app->Controller()->Tag()->unlink($tag->id, $associatedObject->getRef())
            )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE);
            
            $tagLayout = $W->FlowItems(
                $tagLabel,
                $tagDeleteLink
            )->setVerticalAlign('middle')
            ->addClass('app-tag');
                
            if (!$tag->checked) {
                $tagLayout->addClass('app-not-checked');
            }
            $this->addItem(
                $tagLayout
            );
        }
    }
}
