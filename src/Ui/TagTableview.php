<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Tag\Ui;

/**
 *
 * @method Func_App App()
 */
class TagTableview extends \app_TableModelView
{
    protected $tagComponent = null;
    
    public function __construct(\Func_App $app = null, $id = null)
    {
        parent::__construct($app, $id);
        $this->tagComponent = $app->getComponentByName('Tag');
    }
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(\ORM_RecordSet $tagSet = null)
    {
        if (!isset($tagSet)) {
            $tagSet = $this->getRecordSet();
        }
        $this->addColumn(
            app_TableModelViewColumn($tagSet->label)
            );
        $this->addColumn(
            app_TableModelViewColumn($tagSet->description)
            );
        $this->addColumn(
            app_TableModelViewColumn($tagSet->modifiedOn)
            );
        $this->addColumn(
            app_TableModelViewColumn($tagSet->modifiedBy)
            );
        $this->addColumn(
            app_TableModelViewColumn($tagSet->checked)
            );
        $this->addColumn(
            widget_TableModelViewColumn('_actions_', '')
            );
    }
    
    
    /**
     * {@inheritDoc}
     * @see \app_TableModelView::computeCellContent()
     */
    protected function computeCellContent(\ORM_Record $record, $fieldPath)
    {
        $App = $record->App();
        $W = bab_Widgets();
        
        if ($fieldPath == '_actions_') {
            $cellContent = $W->Menu()
            ->setLayout($W->VBoxLayout())
            ->addClass(\Func_Icons::ICON_LEFT_SYMBOLIC);
            if ($record->isUpdatable()) {
                if (!$record->checked) {
                    $cellContent->addItem(
                        $W->Link(
                            $this->tagComponent->translate('Validate'),
                            $App->Controller()->Tag()->validate($record->id)
                        )->addClass('icon', \Func_Icons::ACTIONS_DIALOG_OK)
                        ->setAjaxAction()
                    );
                }
                $cellContent->addItem(
                    $W->Link(
                        $this->tagComponent->translate('Edit'),
                        $App->Controller()->Tag()->edit($record->id)
                    )->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
                );
            }
            if ($record->isDeletable()) {
                $cellContent->addItem(
                    $W->Link(
                        $this->tagComponent->translate('Delete'),
                        $App->Controller()->Tag()->confirmDelete($record->id)
                    )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
                );
            }
            
        } else {
            $cellContent = parent::computeCellContent($record, $fieldPath);
        }
        
        if ($record->checked) {
            $cellContent->addClass('checked');
        } else {
            $cellContent->addClass('not-checked');
        }
        
        return $cellContent;
    }
}
