<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



namespace Capwelton\App\Tag\Ui;


use Capwelton;

class TagUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * @return TagTableView
     */
    public function tableView($id = null)
    {
        return new TagTableview($id);
    }
    
    /**
     * @return TagEditor
     */
    public function editor($id = null, \Widget_Layout $layout = null)
    {
        return new TagEditor($this->app, $id, $layout);
    }
    
    public function displayTags(\app_Record $record, \Widget_Action $tagAction = null, $id = null)
    {
        return $this->TagsListForRecord($record, $tagAction, $id);
    }
    
    public function TagsListForRecord(\app_Record $record, \Widget_Action $tagAction = null, $id = null)
    {
        $W = bab_Widgets();
        
        $App = $record->App();
        
        $tagSet = $App->TagSet();
        $tags = $tagSet->selectFor($record);
        
        $layout = $W->FlowLayout($id);
        $layout->setSpacing(2, 'px');
        
        foreach ($tags as $tag) {
            
            /* @var $tag Capwelton\App\Entry\Set\Tag */
            if ($tag instanceof \app_Link) {
                $tag = $tag->targetId;
            }
            
            $tagLayout = $App->Ui()->Chip($tag->label, null, $App->Controller()->Tag()->unlink($tag->id, $record->getRef()))->addClass('small-chip');
            
            $layout->addItem(
                $tagLayout
            );
        }
        
        return $layout;
    }
    
    
    /**
     * 
     * @param \Func_App $app
     * @param \app_Record $tags
     * @param \app_Record $associatedObject
     * @param \Widget_Action $tagAction
     * @param int $id
     * @return \Capwelton\App\Tag\Ui\TagsDisplay
     */
    public function display($app, $tags, \app_Record $associatedObject, \Widget_Action $tagAction = null, $id = null){
        return new TagDisplay($app, $tags, $associatedObject,$tagAction,$id);
    }
}

/**
 * Display the tags associated to a app_Record.
 *
 * @extends Widget_FlowLayout
 */


