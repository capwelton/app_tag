<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Tag\Ui;


/**
 * @return app_Editor
 */
class TagEditor extends \app_Editor
{
    /**
     * {@inheritDoc}
     * @see \app_Editor::prependFields()
     */
    protected function prependFields()
    {
        $W = $this->widgets;
        $App = $this->App();
        $appC = $App->getComponentByName('Tag');

        $this->isAjax = bab_isAjaxRequest();

        $this->addItem($W->Hidden()->setName('id'));

        $labelFormItem = $this->labelledField(
            $appC->translate('Label'),
            $App->Ui()->SuggestTag()->setSize(60)->setMaxSize(255)
                ->addClass('widget-100pc')
            ->setMandatory(true, $appC->translate('The tag label must not be empty')),
            'label'
        );

        $descriptionFormItem = $this->labelledField(
            $appC->translate('Description'),
            $W->LineEdit()
            ->addClass('widget-100pc'),
            'description'
        );

        $checkedFormItem = $this->labelledField(
            $appC->translate('Checked'),
            $W->CheckBox(),
            'checked'
        );

        $this->addItem($labelFormItem);
        $this->addItem($descriptionFormItem);
        $this->addItem($checkedFormItem);
    }


    /**
     *
     * @param array $tag
     * @param array $namePathBase
     * @return self
     */
    public function setValues($tag, $namePathBase = array())
    {
        if (! ($tag instanceof \app_Record)) {
            return parent::setValues($tag, $namePathBase);
        }

        $values = $tag->getValues();
        parent::setValues($values, $namePathBase);

//         if (! empty($tag->id)) {
//             $this->setHiddenValue('data[id]', $tag->id);
//         }
        return $this;
    }
}
