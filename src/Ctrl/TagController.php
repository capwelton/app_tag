<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Capwelton\App\Tag\Ctrl;
use Capwelton\App\Tag\Widgets;
$App = app_App();
$App->includeRecordController();


/**
 * This controller manages actions that can be performed on tags.
 */
class TagController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Tag'));
    }
    
    /**
     * @param \widget_TableModelView $tableView
     * @return \app_Toolbar
     */
    protected function toolbar(\widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $toolbar = new \app_Toolbar();
        $toolbar->addItem(
            $W->Link(
                $App->getComponentByName('Tag')->translate('Add a tag'), $this->proxy()->edit()
            )->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        
        return $toolbar;
    }
    
    
    
    /**
     *
     * @param array	$data
     * @return bool
     */
    public function save($data = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Tag');
        
        $tagSet = $App->TagSet();
        
        if (isset($data['id']) && !empty($data['id'])) {
            
            // This is an update
            
            $editedTag = $tagSet->request($data['id']);
            
            $sameTag = $tagSet->get($tagSet->label->is($data['label']));
            
            if (isset($sameTag) && $sameTag->id != $editedTag->id) {
                // When we update with an existing tag, we use this existing tag
                // to replace the one being edited
                
                $this->addMessage(sprintf($appC->translate('The "%s" tag has been replaced by the "%s" tag'), $editedTag->label, $sameTag->label));
                
                $tagSet->replaceLinks($editedTag->id, $sameTag->id);
                
                $editedTag->delete();
                return true;
            }
            
        } else {
            
            // This is a creation
            $editedTag = $tagSet->newRecord();
            
        }
        
        $editedTag->setFormInputValues($data);
        $editedTag->save();
        
        $this->addMessage($appC->translate('The tag has been saved'));
        return true;
        //app_redirect(app_BreadCrumbs::last(), $App->translate('The tag has been saved.'));
    }
    
    
    
    /**
     * @param int $id
     * @return boolean
     */
    public function validate($id)
    {
        $tagSet = $this->getRecordSet();
        $tag = $tagSet->request($id);
        if (!$tag->isUpdatable()) {
            
        }
        $tag->checked = true;
        $tag->save();
        
        return true;
    }
    
    
    /**
     * Unlinks the specified tag from the specified app object.
     *
     * @param int		$tag	The tag id.
     * @param string	$from	The app object string reference (e.g. Contact:12).
     * @return \Widget_Action
     */
    public function unlink($tag = null, $from = null)
    {
        $App = $this->App();
        $tagSet = $App->TagSet();
        $tag = $tagSet->get($tag);
        
        $record = $App->getRecordByRef($from);
        
        $tag->unlinkFrom($record);
        
        return true;
    }
    
    
    
    /**
     * @return app_SuggestTag
     */
    public function SuggestTag($id = null)
    {
        $suggest = new Widgets\SuggestTag($this->App(), $id);
        $suggest->setSuggestAction($this->App()->Controller()->Tag()->suggest(), 'search');
        return $suggest;
    }
    
    
    /**
     * Links the specified tag to the specified app object.
     *
     * @param int		$tag	The tag id.
     * @param string	$to		The app object string reference (e.g. Contact:12).
     * @return \Widget_Action
     */
    public function link($tag = null, $to = null)
    {
        $App = $this->App();
        $tagSet = $App->TagSet();
        
        $newTag = $tagSet->get($tagSet->label->is($tag['label']));
        
        if (!isset($newTag)) {
            if (trim($tag['label']) === '') {
                return true;
            }
            $newTag = $tagSet->newRecord();
            $newTag->label = $tag['label'];
            $newTag->save();
        }
        
        $record = $App->getRecordByRef($to);
        
        if (!$newTag->isLinkedTo($record)) {
            $newTag->linkTo($record);
        }
        
        return true;
    }
    
    
    
    
    
    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggest($search = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $suggestTag = $this->SuggestTag();
        $suggestTag->setMetadata('suggestparam', 'search');
        $suggestTag->suggest();
        
        die;
    }
    
    
    
    
    /**
     * Return a visual display of tags associated to the specified object.
     * The selector allows to remove tags and displays an input field to add more.
     *
     * @param string $ref	The app object string reference (e.g. Contact:12)
     * @return \Widget_FlowLayout
     */
    public function selector($ref)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Tag');
        $Ui = $App->Ui();
        
        $record = $App->getRecordByRef($ref);
        if (!isset($record)) {
            return null;
        }
        
        $Ui->includeTag();
        
        $tagsDisplay = $Ui->displayTags($record);
        
        $addTagForm = $W->Form()
        ->setLayout($W->FlowLayout()->setVerticalAlign('middle')->setSpacing(2, 'px'))
        ->setName('tag');
        
        $addTagForm->addItem($Ui->SuggestTag()->setName('label'));
        $addTagForm->addItem(
            $W->SubmitButton()
            ->setAjaxAction($App->Controller()->Tag()->link(), $addTagForm)
            ->setLabel($appC->translate('Add'))
        );
        $addTagForm->setHiddenValue('tg', bab_rp('tg'));
        $addTagForm->setHiddenValue('to', $ref);
        
        $selector = $W->FlowItems(
            $tagsDisplay,
            $addTagForm
        );
        
        return $selector;
    }
}
